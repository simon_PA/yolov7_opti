import os 
import numpy as np
import shutil

os_dir_upper_level = os.path.join(os.getcwd())
object_classes_as_integers = np.linspace(0, 42, num=43).tolist()
percentage_valid_data = 20
data_folder_types = ['images', 'labels']

main_folder = os.path.join(os_dir_upper_level, 'GTRSB_reduced')
# os.mkdir(main_folder)
main_folder_level1_valid = os.path.join(main_folder, 'valid')
main_folder_level1_test = os.path.join(main_folder, 'test')
os.mkdir(main_folder_level1_valid)

for folder in data_folder_types:
    os_dir = os.path.join(os.getcwd(), 'GTRSB_reduced', 'train', folder)
    list_dir = os.listdir(os_dir)
    main_folder = main_folder_level1_valid +'\\' + folder
    os.mkdir(main_folder)
    object_class_idx = 0

    while object_class_idx <= (len(object_classes_as_integers)-1):
        class_items_counter = 0
        class_items_counter_old = class_items_counter
        for filename in list_dir: 
            f = os.path.join(os_dir, filename) 

            
            file = os.path.basename(f)
            indentifier_object_class = int(file[0:5])
            if indentifier_object_class == object_classes_as_integers[object_class_idx]:
                class_items_counter +=1
            if class_items_counter == class_items_counter_old:
            # end of series of one object class reached:
                elements_to_split_object_class = round(class_items_counter * (percentage_valid_data/100.0), 0) 
                break
            class_items_counter_old = class_items_counter   
        safe_idx = 1

        for filename in list_dir:
            f = os.path.join(os_dir, filename) 
            file = os.path.basename(f)
            #print(filename_without_type)
            filename_for_png =main_folder  + '\\' + file
            shutil.copyfile(f, filename_for_png)
            os.remove(f)
            if safe_idx >= elements_to_split_object_class:
                break
            safe_idx +=1

        object_class_idx += 1
        del list_dir[0:class_items_counter] 




             